package junitexample;

/**
 *
 * @author Sina
 */
public class PrintGrade {
    public String print(int grade) {
        if (grade > 93) {
            return "You got an A";
        } else if (grade > 83) {
            return "You recieved a B";
        } else if (grade > 73) {
            return "You recieved a C";
        } else
            return "You need to study more";
    }
}
