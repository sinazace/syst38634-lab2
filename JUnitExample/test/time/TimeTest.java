/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package time;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static junit.framework.Assert.fail;
import static org.junit.Assert.assertFalse;
import org.junit.Test;
/**
 *
 * @author Sina Lyon
 */
public class TimeTest {
    
    @Test
    public void testGetTotalMilliSecondsBoundaryOut() {
        int totalMilliseconds = Time.getTotalMilliSeconds("12:05:50:1000");
        assertFalse("Invalid number of milliseconds", totalMilliseconds != 1000);
    }
    
    @Test
    public void testGetTotalMilliSecondsBoundaryIn() {
        int totalMilliseconds = Time.getTotalMilliSeconds("12:05:50:999");
        assertTrue("Invalid number of milliseconds", totalMilliseconds == 999);
    }
    
    @Test 
    public void testGetTotalMilliSecondsException() {
        int totalMilliseconds = Time.getTotalMilliSeconds("12:05:50:05");
        assertFalse("Invalid number of milliseconds", totalMilliseconds > 999);
    }
    
        //    NOTE: for the exception test method, you can specify what exception is expected using the annotation.
//(expected = NumberFormatException.class)
    // Milliseconds code
    @Test
    public void testGetTotalMilliSecondsRegular() {
        int totalMilliseconds = Time.getTotalMilliSeconds("12:05:50:05");
        assertTrue("Invalid number of milliseconds", totalMilliseconds == 5);
    }
    

    
//    @Test
//    public void testGetTotalMilliSecondsBoundaryIn() {
////                fail("Invalid number of milliseconds");
//        int totalMilliseconds = Time.getTotalMilliSeconds("12:05:50:999");
//        assertTrue("Invalid number of milliseconds", totalMilliseconds == 999);
//    }
//    

    
    // seconds code
//    @Test
//    public void testGetTotalSecondsRegular() {
//        int totalSeconds = Time.getTotalSeconds("01:01:01");
//        assertEquals("The time provided does not match the result", totalSeconds == 3661);
//    }
//    
//    @Test
//    public void testGetTotalSecondsException() {
//        int totalSeconds = Time.getTotalSeconds("01:01:0A");
//        fail("The time provided is not valid");
//    }
//    
//    @Test
//    public void testGetTotalSecondsBoundaryIn() {
//        int totalSeconds = Time.getTotalSeconds("00:00:59");
//        assertEquals("The time provided does not match the result", totalSeconds == 59);
//    }
//    
//    @Test
//    public void testGetTotalSecondsBoundaryOut() {
//        int totalSeconds = Time.getTotalSeconds("00:00:60");
//        fail("The time provided is not valid");
//    }
}
