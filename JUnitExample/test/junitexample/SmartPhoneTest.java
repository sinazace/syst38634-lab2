/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package junitexample;

import org.junit.Test;
import phone.SmartPhone;
import phone.VersionNumberException;
import static org.junit.Assert.*;

/**
 *
 * @author Sina Lyon
 */
public class SmartPhoneTest {
    
    @Test
    public void testGetFormattedPriceRegular() {
        String formattedPrice = "$20";
        SmartPhone instance = new SmartPhone();
        instance.setPrice(20.00);
        assertEquals(instance.getFormattedPrice(), formattedPrice);
    }
    @Test
    public void testGetFormattedPriceException() {
        String formattedPrice = "20.$%0";
        SmartPhone instance = new SmartPhone();
        instance.setPrice(20.00);
        fail("Price not in CAD format.");
    }
    
    @Test
    public void testGetFormattedPriceBoundaryIn() {
        String formattedPrice = "$20";
        SmartPhone instance = new SmartPhone();
        instance.setPrice(20);
        assertEquals(instance.getFormattedPrice(), formattedPrice);
    }
    
    @Test
    public void testGetFormattedPriceBoundaryOut() {
        String formattedPrice = "20.00";
        SmartPhone instance = new SmartPhone();
        instance.setPrice(20.00);
        fail("Price not in CAD format.");
    }
    
    
    @Test
    public void testSetVersionRegular() throws VersionNumberException {
        System.out.println("setVersion");
        double version = 2.6;
        SmartPhone instance = new SmartPhone();
        instance.setVersion(version);
        assertEquals(instance.getVersion(), version);
    }
    
    @Test
    public void testSetVersionException() throws VersionNumberException {
        System.out.println("setVersion");
        double version = -8;
        SmartPhone instance = new SmartPhone();
        instance.setVersion(version);
        fail("Not an appropriate version number.");
    }
    
    @Test
    public void testSetVersionBoundaryIn() throws VersionNumberException {
        System.out.println("setVersion");
        double version = 1.0;
        SmartPhone instance = new SmartPhone();
        instance.setVersion(version);
        assertEquals(instance.getVersion(), version);
    }
    
    @Test
    public void testSetVersionBoundaryOut() throws VersionNumberException {
        System.out.println("setVersion");
        double version = 0.9;
        SmartPhone instance = new SmartPhone();
        instance.setVersion(version);
        fail("Not an appropriate version number.");
    }
    
}
