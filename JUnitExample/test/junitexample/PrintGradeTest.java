package junitexample;

//import org.junit.jupiter.api.Test;
//import static org.junit.jupiter.api.Assertions.*;
import static junit.framework.Assert.assertEquals;
import org.junit.Test;
import org.junit.*;

/**
 *
 * @author Sina
 */
public class PrintGradeTest {

    /**
     * Test of print method, of class PrintGrade.
     */
    @Test
    public void testPrint() {
        PrintGrade pg = new PrintGrade();
        String result = pg.print(95);
        assertEquals("You got an A", result); // needs expected result & actual result as arguments
    }
    
}
